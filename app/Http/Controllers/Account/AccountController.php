<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Formatter\ResponseFormatter;
use App\Http\Requests\Account\UpdatePasswordRequest;
use App\Http\Requests\Account\UpdateProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        $user = User::whereId($request->user()->id)
            ->get();

        if ($user)
            return ResponseFormatter::success($user, "Get profile successfully!");
        else
            return ResponseFormatter::error(null, "Failed to get the profile!");
    }

    public function update(UpdateProfileRequest $request)
    {
        $user = User::findOrFail($request->user()->id);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        if ($request->hasFile('photo')) {
            $file = $request->file('photo');

            $user['photo'] = $file->storeAs(
                'users-photos',
                $request->user()->id . "." . $file->getClientOriginalExtension(),
                'public'
            );

            $user->save();
        }

        return ResponseFormatter::success($user, "Data added successfully!");
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = User::findOrFail($request->user()->id);

        if ( !Hash::check($request->current_password, $user->password) ) {
            return ResponseFormatter::error(null, "The current password you entered is incorrect!");

        } else {
            $user->update([
                'password' => Hash::make($request->password)
            ]);

            return ResponseFormatter::success($user, "Update password successfully!");
        }
    }
}
