<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Formatter\ResponseFormatter;
use App\Models\OtpCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    public function verification(Request $request)
    {
        $otpCode = OtpCode::whereOtpCode($request->otp_code)
            ->first();

        if ( !empty($otpCode) ) {

            if ( Carbon::now() > $otpCode->expired_at )
                return ResponseFormatter::error(null, "The verification code has expired, please generate it again!", 400);

            else
                $user = User::whereId($otpCode->user_id)
                    ->update([
                        'email_verified_at' => Carbon::now()
                    ]);

                $otpCode->delete();

                return ResponseFormatter::success($user, "Congratulations, verification was successful!");

        } else {
            return ResponseFormatter::error(null, "The verification code you entered is incorrect!", 400);

        }
    }
}
