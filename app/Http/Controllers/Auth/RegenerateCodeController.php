<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Formatter\ResponseFormatter;
use App\Models\OtpCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RegenerateCodeController extends Controller
{
    public function regenerateCode(Request $request)
    {
        $user = User::whereEmail($request->email)
            ->first();

        $optCode = OtpCode::whereUserId($user->id)
            ->update([
                'otp_code' => Str::upper(Str::random(6)),
                'expired_at' => Carbon::now()->addMinutes(5)
            ]);

        return ResponseFormatter::success($optCode, "Please check your email!", 200);
    }
}
