<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UseUuid;

class Role extends Model
{
    use HasFactory, UseUuid;

    protected $table = 'roles';

    protected $fillable = [
        'role_name',
    ];

    public function user()
    {
        return $this->hasMany(User::class, 'id');
    }

}
